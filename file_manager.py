# Your code goes here.
import json


def count(filename) -> int:
    """Count the number of records available in the JSON file"""
    number_of_films = None
    with open(filename, "r", encoding="utf-8") as file:
        list_of_films = json.load(file)
        print(type(list_of_films))
        number_of_films = len(list_of_films)
    return number_of_films


def written_by(filename, writer) -> list:
    result = []
    with open(filename, "r", encoding="utf-8") as file:
        records = json.load(file)
        for i in range(len(records)):
            print(records[i]['Writer'])
            if records[i]['Writer'] == writer:
                result.append(records[i])
                print(records[i]['Writer'])
    return result


def longest_title(filename) -> str:
    with open(filename, "r", encoding="utf-8") as file:
        table_3 = json.load(file)
        for i in range(len(table_3)):
            title_of_film = str(table_3)
    return title_of_film